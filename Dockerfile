#基础镜像
FROM ubuntu:14.04
#设置运行文件夹
RUN mkdir /app
#复制二进制文件到工作目录
COPY exhaust_cpu /app/
#设置工作目录
WORKDIR /app
#导入执行脚本
ADD run.sh /app/run.sh
RUN chmod 755 /app/run.sh
#执行脚本
CMD ["/app/run.sh"]